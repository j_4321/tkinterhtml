``tkinterhtml``
===============
Fork of https://bitbucket.org/aivarannamaa/tkinterhtml
This is Python wrapper for `Tkhtml3 <http://tkhtml.tcl.tk/>`_.


Includes Python package ``tkinterhtml`` with class ``TkinterHtml``

Dependencies
------------

* Python 3
* Tkinter
* `Tkhtml3 <http://tkhtml.tcl.tk/>`__

`build_tkhtml_unix.sh` can be used to compile Tkhtml3 on linux.

Usage
-----
See `tkinterhtml/__main__.py <https://bitbucket.org/j_4321/tkinterhtml/src/master/tkinterhtml/__main__.py>`_.
