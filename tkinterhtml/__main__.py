import urllib.request
import tkinter as tk

from tkinterhtml import HtmlFrame

root = tk.Tk()

frame = HtmlFrame(root, horizontal_scrollbar="auto", width=200, height=400)
frame.pack(fill='both', expand=True)

frame.set_content("""
<html>
<body>
<h1>Hello world!</h1>
<p>First para</p>
<ul>
    <li>first list item</li>
    <li>second list item</li>
</ul>
<img src="http://findicons.com/files/icons/638/magic_people/128/magic_ball.png"/>
<a href="https://bitbucket.org/j_4321/tkinterhtml">Link</a>
</body>
</html>
""")

frame.set_style("""
a {
color: #0000FF;
font-style: italic;
}

a:hover
{
border-bottom: 1px solid #0000FF;
}
""")

root.mainloop()
