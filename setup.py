from setuptools import setup

url = "https://bitbucket.org/j_4321/tkinterhtml"

setup(
      name="tkinterhtml",
      version="0.7-fork0.1",
      description="Python wrapper for Tkhtml3 (http://tkhtml.tcl.tk/)",
      long_description="See %s for more info" % url,
      url=url,
      license="MIT",
      classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development",
      ],
      keywords="tkinter Tkinter tkhtml Tkhtml Tk",
      packages=["tkinterhtml"],
)